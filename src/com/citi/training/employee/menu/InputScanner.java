package com.citi.training.employee.menu;

public interface InputScanner {
	int getInt(String promptMsg, String errorMsg);
	double getDouble(String promptMsg);
	String getString(String promptMsg);
	void clearScanner();

}
